1.  Create a single HTML document named “index.html” with all appropriate sections.

2.  Include the title (“Assignment #X – LastName, FirstName”) at the top of the page using a "h1" tag.  This must be centered.

3.  For all "table" tags, add the attribute border=1

4.  Add a menu directly below the title at the top of the page linking to each section.

5.  Add a “Back to Top” link at the end of each section that links back to the "h1" tag used in the main title
• Section 1 - If you do not shop regularly, be creative in the items you include.
• Section 2 – Create an unordered list of at least 4 states.  For each state, add a nested listed consisting of at least 4 cities in that state.
• Section 3 – Create a definition list consisting of your 5 favorite movies with a brief synopsis of each. 
• Section 4 – Create a table listing the details of various trees.  The table will have 4 headings:  “Pine”, “Maple”, “Oak”, and “Fir”.  Add a row above these headings that spans all 4 headings with a value of “Tree Stats”.  For each type of tree you need to include a labeled row for “Average Height”, “Average Width”, and “Typical Life Span”. You may make up all data values.
• Section 5 – Using your five favorite states, create an appropriate table that lists the state, state bird, state flower, and state tree. Feel free to use states of any country.
• Section 6 – Create a menu according to the table below. Feel free to substitute foods as you see fit.